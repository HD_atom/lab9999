#include "TestStack.h"
#include "Stack.h"
#include <vector>


bool TestStack::TestIntStack()
{
	Stack<int> stack;
	stack.Push(10);
	stack.Push(20);
	stack.Push(30);
	stack.Push(40);
	stack.Push(50);
	stack.Push(60);
	stack.Push(70);
	std::vector<int> expectedOutput = { 70,60,50,40,30,20,10 };
	std::vector<int> testOutput;
	testOutput.push_back(stack.Pop());
	testOutput.push_back(stack.Pop());
	testOutput.push_back(stack.Pop());
	testOutput.push_back(stack.Pop());
	testOutput.push_back(stack.Pop());
	testOutput.push_back(stack.Pop());
	testOutput.push_back(stack.Pop());
	for (int i = 0; i < testOutput.size(); i++)
	{
		if (testOutput[i] != expectedOutput[i])
			return false;
	}
	return true;

}


